console.log("Hello World");

//Arithmetic Operators
// +, -, *, /, %

let x = 10;
let y = 3;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment operator
// Basic assignment operator is (=) equal sign

let assignmentNumber = 8;

// addition assignment
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

//  Multiple Operators and Parenthesis
//

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Incrementation and decrementation

let z = 1;

// pre-incrementation
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// post-incrementation
increment = z++;
console.log("Result of post-increment: " + increment);


//decrementation
//pre-decrementation
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// post-decrementation
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

//Type coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// true = 1
let numE = true + 1;
console.log(numE);

// false = 0
let numF = false + 1;
console.log(numF);

// Comparison operator
let juan = "juan";

//Equality operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality operator (!=)
// ! = not
console.log(1 != 1);
console.log(1 != 2);

// Strict equality operator (===)
console.log(1 === '1');
console.log("juan" === juan);
console.log(0 === false);

// Strict Inequality operator (!==)
console.log(1 !== '1');
console.log("juan" !== juan);
console.log(0 !== false);

// Relation operator
//  >, <, =
let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// Logical operators
// && --> AND, || --> OR, ! --> NOT

let isLegalAge = true;
let isRegistered = false;

letAllRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: " + letAllRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical OR Result: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Logical NOT Result: " + someRequirementsNotMet);
